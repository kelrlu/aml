import os
import re
import csv
import nltk # remember to install nltk and download punkt from nltk.download()
import numpy as np
import nltk.data
import sys
import pickle
import re
csv.field_size_limit(sys.maxsize)

# takes a file where each row: rowname, matriE
def build(src_filename, delimiter='\n', header=True, quoting=csv.QUOTE_NONE):    
    # Thanks to Prof. Chris Potts for this function
    reader = csv.reader(file(src_filename), delimiter=delimiter, quoting=quoting)
    colnames = None
    if header:
        colnames = reader.next()
        colnames = colnames[1: ]
    mat = []    
    rownames = []
    for line in reader: 
    	
    	# print "line1:",line
    	# #line =line[0].split(" ")
    	# print "line2:",line
    	# print "rowname:", line[0]   
        rownames.append(line[0])

        # print "mat:", line[1:]
        mat.append(np.array(map(float, line[1: ])))
    word2idx = {x: id for id,x in enumerate(rownames)}
    idx2word = {id: w for w, id in word2idx.iteritems()}
    return (np.array(mat), word2idx, idx2word, colnames)

def get_data(return_type="wv"):
	GLOVE_MAT, WORD2IDX, IDX2WORD, _ = build('/Users/kelurulu/Documents/Classes/Spring 2017/Advanced Machine Learning/Project/aml/src/glove/glove.6B.50d.txt', delimiter=' ', header=False, quoting=csv.QUOTE_NONE)
	path = '/Users/kelurulu/Documents/Classes/Spring 2017/Advanced Machine Learning/Project/aml/src/data_processing/wiki/'
	all_wiki_files = os.listdir(path)
	all_wiki_files = all_wiki_files[1:] # to remove .DS_Store
	tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
	all_wiki_lines = []
	for wiki_file in all_wiki_files:
		all_lines = open(path + wiki_file, 'r').readlines()
		for line in all_lines:
			if line[0] != "<":
				line = line.replace('\n', '')
				if line != "":
					all_wiki_lines.append(line)
	all_sentences = []
	for line in all_wiki_lines:
		try:
			sentences = tokenizer.tokenize(line) 
		except:
			pass
		sentences_missed = 0
		for sentence in sentences:
			sentence = re.sub(r'[^\x00-\x7F]+',' ', sentence)
			try:
				new_sentence = ['<s>']
				tokens = nltk.word_tokenize(sentence)
				for token in tokens:
					token = token.lower()
					new_sentence.append(IDX2WORD[WORD2IDX[token]])
				new_sentence.append('</s>')
				all_sentences.append(" ".join(new_sentence))
			except:
				sentences_missed += 1
				pass
			#all_sentences.append('<s> ' + sentence.lower() + ' </s>')
	# if return_type == "char":
	# 	return all_sentences
	# all_wvi = []
	# num_sentences = len(all_sentences)
	# sentences_missed = 0
	# num_seen = 0
	# for sentence in all_sentences:
	# 	try:
	# 		wvi_sentence = []
	# 		tokens = nltk.word_tokenize(sentence)
	# 		for token in tokens:
	# 			token = token.lower()
	# 			wvi_sentence.append(GLOVE_VOCAB.index(token))
	# 		all_wvi.append(wvi_sentence)
	# 	except:
	# 		sentences_missed += 1
	# 		pass
	# 	num_seen += 1
	# 	if num_seen % 10000 == 0:
	# 		print "Seen: %d" % num_seen
	# print "Number of sentences obtained: %d" % (num_sentences - sentences_missed)
	# print "Total number of sentences: %d" % num_sentences
	# return all_wvi
	with open('/Users/kelurulu/Documents/Classes/Spring 2017/Advanced Machine Learning/Project/aml/src/data/wiki.src','w+') as f:
		f.write('\n'.join(all_sentences))

	opath = '/Users/kelurulu/Documents/Classes/Spring 2017/Advanced Machine Learning/Project/aml/src/data'
	# dump out glove files
	with open(os.path.join(opath, 'glove_mat.pkl'),'wb') as f:
		pickle.dump(GLOVE_MAT,f)

	with open(os.path.join(opath, 'word2idx.pkl'),'wb') as g:
		pickle.dump(WORD2IDX,g)

	with open(os.path.join(opath, 'idx2word.pkl'),'wb') as h:
		pickle.dump(IDX2WORD,h)

	return all_sentences

s = get_data()