import codecs
import os
import sys
from collections import defaultdict
sys.path.insert(0, 'data_processing')
from wiki_to_vectors import *

class Corpus:
    """Simple Bitext corpus reader."""
    def __init__(self, source_file, target_file = None):
        self.source_sentences, self.source_sentences_maxlen = self.read_corpus(source_file)
        self.target_sentences, self.target_sentences_maxlen = self.read_corpus(target_file)
        self.source_word2idx, self.source_idx2word, self.source_glove_mat = self.get_dictionaries(self.source_sentences)
        self.target_word2idx, self.target_idx2word, self.target_glove_mat = self.get_dictionaries(self.target_sentences)

    def read_corpus(self, file_name):
        if file_name is None:
            return (None, 0)
        sentences = []
        maxlen = 0
        with codecs.open(file_name, "r", "utf-8") as fh:
            for line in fh:
                words = line.strip().split()
                maxlen = max(maxlen, len(words))
                sentences.append(words)
        return (sentences, maxlen)

    def write_corpus(self, file_name, sentences):
        with codecs.open(file_name + '.new', "w", "utf-8") as fh:
            for sentence in sentences:
                fh.write(' '.join(sentence))
                fh.write('\n')

    def get_dictionaries(self, sentences):
        # GLOVE_MAT, WORD2IDX, IDX2WORD, _ = build('/Users/kelurulu/Documents/Classes/Spring 2017/Advanced Machine Learning/Project/aml/src/glove/glove.6B.50d.short.txt', delimiter=' ', header=False, quoting=csv.QUOTE_NONE)

        # if sentences is None:
        #     return None
        # word2idx = defaultdict(int)
        # idx = 0
        # for sentence in sentences:
        #     for word in sentence:
        #         if word not in word2idx:
        #             word2idx[word] = idx
        #             idx += 1
        # idx2word = {id: w for w, id in word2idx.iteritems()}
        print 'Reading in pickle files...'
        opath = '/Users/kelurulu/Documents/Classes/Spring 2017/Advanced Machine Learning/Project/aml/src/data'
        WORD2IDX = pickle.load( open( os.path.join(opath, "word2idx.pkl"), "rb" ))
        IDX2WORD = pickle.load( open( os.path.join(opath, "idx2word.pkl"), "rb" ))
        GLOVE_MAT = pickle.load( open( os.path.join(opath, "glove_mat.pkl"), "rb"))

        return (WORD2IDX, IDX2WORD, GLOVE_MAT)
