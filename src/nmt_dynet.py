import sys
sys.path.insert(0, 'data_processing')
sys.path.insert(0, 'data')
from wiki_to_vectors import *

from dynet import *
import argparse
from utils import Corpus
import random
import numpy as np
from bleu import get_bleu_score
import json
import pickle

RNN_BUILDER = GRUBuilder

class nmt_dynet:

    def __init__(self, src_vocab_size, tgt_vocab_size, src_word2idx, src_idx2word, tgt_word2idx, tgt_idx2word, word_d, gru_d, gru_layers, embeddings_init):
        '''
        initialization method for nmt_dynet

        ---Parameters---

        src_vocab_size (int): vocab size in the source language
        tgt_vocab_size (int): vocab size in the target language
        src_word2idx (dict): a dict mapping words to indices for the source language
        src_idx2word (dict): a key/value reversed dict of src_word2idx
        tgt_word2idx (dict): a dict mapping words to indices for the target language
        tgt_idx2word (dict): a key/value reversed dict of tgt_idx2word
        word_d (int): dimention of the input/word embedding
        gru_d (int): dimention of the hidden states
        gru_layers (int): number of layers
        
        ---Returns---

        None
        
        ---Implementation Details---
        
        You should replace None(s) with initializations below
        fwd_RNN/bwd_RNN: they are the same excepts the input word order (initializations are the same.).
                         refer to encoder folumas setting the input_dim/hidden_dim in page 24 in Karl's ppt.
                         the input for encoder networks is word embedding e_x{i}.
        dec_RNN: refer to decoder folumas setting the input_dim/hidden_dim in page 25 in Karl's ppt.
                 the inputs for decoder networks are word embedding e_y{i-1} AND the hidden state 
                 of the last word from encoder networks h_m (Remeber we are using bidirectional encoder,
                 you need to concatenate word embedding, the two hidden states of the last words from 
                 fwd_RNN/bwd_RNN. This help you decide the value of input_dim/hidden_dim).
        output_w/output_b: output weight matrix and bias vector
        '''
        # initialize variables
        self.gru_layers = gru_layers
        self.src_vocab_size = src_vocab_size
        self.tgt_vocab_size = tgt_vocab_size
        self.src_word2idx = src_word2idx
        self.src_idx2word = src_idx2word
        self.tgt_word2idx = tgt_word2idx
        self.tgt_idx2word = tgt_idx2word
        self.word_d = word_d
        self.gru_d = gru_d

        self.model = Model()

        # the embedding paramaters
        # self.source_embeddings = self.model.add_lookup_parameters((self.src_vocab_size, self.word_d))
        # self.target_embeddings = self.model.add_lookup_parameters((self.tgt_vocab_size, self.word_d))
        embeddings_all = inputTensor(embeddings_init)
        self.source_embeddings = embeddings_all
        print '<s> source embedding:', self.source_embeddings[400000].npvalue
        self.target_embeddings = embeddings_all
        print '<s> target embedding:', self.target_embeddings[400000].npvalue


        # YOUR IMPLEMENTATION GOES HERE
        # project the decoder output to a vector of tgt_vocab_size length
        self.output_w = self.model.add_parameters((self.tgt_vocab_size,self.gru_d))
        self.output_b = self.model.add_parameters((self.tgt_vocab_size,))

        # encoder network
        # the foreword rnn
        self.fwd_RNN = GRUBuilder(self.gru_layers, self.word_d, self.gru_d, self.model)
        # the backword rnn
        self.bwd_RNN = GRUBuilder(self.gru_layers, self.word_d, self.gru_d, self.model)

        # decoder network
        self.dec_RNN = GRUBuilder(self.gru_layers, 2*self.gru_d  + self.word_d, self.gru_d, self.model)


    def encode(self, src_sentence):
        '''
        src_sentence: list of words in the source sentence (i.e output of .strip().split(' '))
        return encoding of the source sentence

        compute the hidden state of the last word in fwd_RNN/bwd_RNN for one source sentence

        ---Parameters---

        src_sentence (list): list of indices/words (depends on your implementation) in the source sentence
        
        ---Returns---

        h_m (vector): encoding of the source sentence/the hidden state of the last word from encoder networks 
                      h_m in page 25/28 in Karl's ppt (Again, we are using bidirectional encoder, you need to
                      concatenate the hidden states of the last words from fwd_RNN and bwd_RNN)
                      
        ---Implementation Details---
        
        To get the hidden states in fwd_RNN/bwd_RNN, refer to 
        1. The LSTM (RNN) Interface section in http://dynet.readthedocs.io/en/latest/tutorials_notebooks/RNNs.html
        '''
        # YOUR IMPLEMENTATION GOES HERE

        fwd_s = self.fwd_RNN.initial_state()
        embed_sentence = []
        # print src_sentence

        for word in src_sentence:
            word_embedding = self.source_embeddings[self.src_word2idx[word]]
            # print 'Word embedding',word_embedding.npvalue
            embed_sentence.append(word_embedding)
            fwd_s = fwd_s.add_input(word_embedding)
            # print 'after fwd'

        bwd_s = self.bwd_RNN.initial_state()
        for word_embedding in embed_sentence[::-1]:
            # print 'in bwd'
            bwd_s = bwd_s.add_input(word_embedding)
        #     print 'after bwd'

        # print 'outside bwd'
        return concatenate([fwd_s.output(), bwd_s.output()])


    def get_loss(self, src_sentence, tgt_sentence):
        '''
        src_sentence: words in src sentence
        tgt_sentence: words in tgt sentence
        return loss for this source target sentence pair
        
        compute loss of RNN for one source/target pair sentence

        ---Parameters---

        src_sentence (list): list of words in the source sentence
        tgt_sentence (list): list of words in the target sentence
        
        ---Returns---

        loss (DyNet Expression object): loss of RNN for one source/target pair sentence
                                        can use loss.value() to get the real loss value
                      
        ---Implementation Details---
        
        To calculate the loss, refer to 
        1. 'do_one_sentence' function in the part1.py from the assignment 3
        2. 'create_network_return_loss' function in http://dynet.readthedocs.io/en/latest/tutorials_notebooks/API.html
        
        use softmax to get probs of all target words, and get the predicted prob of the gold word in the tgt_sentence
        '''
        renew_cg()

        W = parameter(self.output_w)
        b = parameter(self.output_b)

        h_m = self.encode(src_sentence)
        
        # print 'after encode'
        s = self.dec_RNN.initial_state()
        
        prev_embed = self.target_embeddings[self.tgt_word2idx['<s>']]
        # print 'target_embeddings', self.target_embeddings
        # print 'tgt_word2idx:',self.tgt_word2idx['<s>']
        # print 'after prev_embed'
        # prev_embed = vecInput(self.word_d)
        # prev_embed.set([0 for i in range(self.word_d)])

        loss = []
        for index,word in enumerate(tgt_sentence[1:]):
            print 'prev_embed: ',prev_embed.npvalue()
            # print ' in for - index,word:', index, word
            #word_embed = self.target_embeddings[self.tgt_word2idx[word]]
            curr_word_idx = self.tgt_word2idx[word]
            print 'word: ',tgt_idx2word[curr_word_idx]
            # print 'curr_word_idx:', curr_word_idx
            # print 'hm:', h_m.npvalue()
            # print 'hmshape:',h_m.npvalue().shape
            net_input = concatenate([prev_embed, h_m])
            # print 'net input',net_input.dim()
            # print 'length', net_input
            s = s.add_input(net_input)
            # print 'after s add input'
            h = s.output()
            # print 'after h output'

            probs = softmax(W*h + b)
            # print 'probls', probs
            # print 'probs[curr_word_idx]',probs[curr_word_idx]
            loss.append( -log(probs[curr_word_idx]))
            # print 'after loss funct'

            prev_embed = self.target_embeddings[curr_word_idx]
        
        loss = esum(loss)
        print 'sentence:', src_sentence
        print 'sentence loss:', loss.npvalue()
        return loss 


    def generate(self, src_sentence):
        '''
        src_sentence: list of words in the source sentence (i.e output of .strip().split(' '))
        return list of words in the target sentence

        run trained encoder_decoder model to predict/generate/translate one source sentence

        ---Parameters---

        src_sentence (list): list of words in the source sentence
        
        ---Returns---

        tgt_sentence (list): list of predicted words in the target sentence
                      
        ---Implementation Details---
        
        To generate, refer to 
        1. 'generate' function in the part1.py from the assignment 3
        2. 'create_network_return_best' function in http://dynet.readthedocs.io/en/latest/tutorials_notebooks/API.html
        
        '''
        renew_cg()

        # YOUR IMPLEMENTATION GOES HERE
        W = parameter(self.output_w)
        b = parameter(self.output_b)

        h_m = self.encode(src_sentence)
        
        s = self.dec_RNN.initial_state()

        tgt_sentence = [self.tgt_word2idx['<s>']]

        while True:
            prev_embed = self.target_embeddings[tgt_sentence[-1]]
            net_input = concatenate([prev_embed, h_m])
            s = s.add_input(net_input)
            h = s.output()

            probs = softmax(W*h + b)
            y = np.argmax(probs.vec_value())
            tgt_sentence.append(y)

            if self.tgt_idx2word[tgt_sentence[-1]] == "</s>" or len(tgt_sentence) > (len(src_sentence) * 2): 
                break

        for i in range(len(tgt_sentence)):
            tgt_sentence[i] = self.tgt_idx2word[tgt_sentence[i]]

        print 'target_sent:' + " ".join(tgt_sentence)
        return tgt_sentence 


    def translate_all(self, src_sentences):
        translated_sentences = []
        for src_sentence in src_sentences:
            # print src_sentence
            translated_sentences.append(self.generate(src_sentence))

        return translated_sentences

    # save the model, and optionally the word embeddings
    def save(self, filename):

        self.model.save(filename)
        embs = {}
        if self.src_idx2word:
            src_embs = {}
            for i in range(self.src_vocab_size):
                src_embs[self.src_idx2word[i]] = self.source_embeddings[i].value()
            embs['src'] = src_embs

        if self.tgt_idx2word:
            tgt_embs = {}
            for i in range(self.tgt_vocab_size):
                tgt_embs[self.tgt_idx2word[i]] = self.target_embeddings[i].value()
            embs['tgt'] = tgt_embs

        if len(embs):
            with open(filename + '_embeddings.json', 'w') as f:
                json.dump(embs, f)


def get_val_set_loss(network, val_set):
        loss = []
        for src_sentence, tgt_sentence in zip(val_set.source_sentences, val_set.target_sentences):
            loss.append(network.get_loss(src_sentence, tgt_sentence).value())

        return sum(loss)

def main(train_src_file, train_tgt_file, dev_src_file, dev_tgt_file, model_file, num_epochs, embeddings_init = None, seed = 0):
    print 'epochs',num_epochs
    ####CHANGING
    # word_vector_file = "glove/glove.6B.50D.short.txt"
    # # word_vector_file = "data/vectors.6B.200d.txt"
    # ####
    # wiki_files_directory = "data_processing/wiki/"
    # sentences_file = 'data/all_wvi.pkl'

    # print 'Building word vectors'
    # #### CHANGING TO BUILD
    # glove_mat, glove_rownames, glove_colnames = build(word_vector_file,header=False) # Matrix of shared theano tensor vectors
    # print 'MAT', glove_mat
    # print 'ROW', glove_rownames
    # print 'COL',glove_colnames
        ####
    # print 'Building sentences'
    # if sentences_file is not None:
    #     sentences = pickle.load(open('data/all_wvi.pkl', 'r'))
    # else:
    #     #### CHANGING TO GET_DATA
    #     sentences = get_data() # num_sentences x index into word in dictionary
    #     ####
    # sentences = map(np.array, sentences)
    # train_sentences = sentences[:int(0.8*len(sentences))]


    print('reading train corpus ...')
    train_set = Corpus(train_src_file, train_tgt_file)
    # assert()
    print('reading dev corpus ...')
    dev_set = Corpus(dev_src_file, dev_tgt_file)

    print 'Initializing simple neural machine translator:'
    # src_vocab_size, tgt_vocab_size, tgt_idx2word, word_d, gru_d, gru_layers
    encoder_decoder = nmt_dynet(len(train_set.source_word2idx), len(train_set.target_word2idx), train_set.source_word2idx, train_set.source_idx2word, train_set.source_word2idx, train_set.source_idx2word, 50, 50, 2, train_set.source_glove_mat)

    trainer = SimpleSGDTrainer(encoder_decoder.model)

    sample_output = np.random.choice(len(dev_set.target_sentences), 5, False)
    losses = []
    best_bleu_score = 0

    for epoch in range(num_epochs):
        print 'Starting epoch', epoch
        # shuffle the training data
        combined = list(zip(train_set.source_sentences, train_set.target_sentences))
        random.shuffle(combined)
        train_set.source_sentences[:], train_set.target_sentences[:] = zip(*combined)

        opath = '/Users/kelurulu/Documents/Classes/Spring 2017/Advanced Machine Learning/Project/aml/src/data/'
        if(!os.path.exists(os.path.join(opath,'trained_model'))):
            print 'Training . . .'
            sentences_processed = 0
            for src_sentence, tgt_sentence in zip(train_set.source_sentences, train_set.target_sentences):
                loss = encoder_decoder.get_loss(src_sentence, tgt_sentence)
                loss_value = loss.value()
                loss.backward()
                trainer.update()
                sentences_processed += 1
                if sentences_processed % 4000 == 0:
                    print 'sentences processed: ', sentences_processed
            encoder_decoder.save(os.path.join(opath, "trained_model"))
        else:


        # Accumulate average losses over training to plot
        val_loss = get_val_set_loss(encoder_decoder, dev_set)
        print 'Validation loss this epoch', val_loss
        losses.append(val_loss)

        print 'Translating . . .'
        translated_sentences = encoder_decoder.translate_all(dev_set.source_sentences)

        print('translating {} source sentences...'.format(len(sample_output)))
        for sample in sample_output:
            print('Target: {}\nTranslation: {}\n'.format(' '.join(dev_set.target_sentences[sample]),
                                                                         ' '.join(translated_sentences[sample])))

        bleu_score = get_bleu_score(translated_sentences, dev_set.target_sentences)
        print 'bleu score: ', bleu_score
        if bleu_score > best_bleu_score:
            best_bleu_score = bleu_score
            # save the model
            encoder_decoder.save(model_file)

    print 'best bleu score: ', best_bleu_score



if __name__ == '__main__':

    parser = argparse.ArgumentParser(description = '')
#     parser.add_argument('model_type')
    parser.add_argument('train_src_file')
    parser.add_argument('train_tgt_file')
    parser.add_argument('dev_src_file')
    parser.add_argument('dev_tgt_file')
    parser.add_argument('model_file')
    parser.add_argument('--num_epochs', default = 20, type = int)
    parser.add_argument('--embeddings_init')
    parser.add_argument('--seed', default = 0, type = int)
    parser.add_argument('--dynet-mem')

    args = vars(parser.parse_args())
    args.pop('dynet_mem')

    main(**args)
